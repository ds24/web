var db = require('./models');
var colors = require('colors');

db.sequelize.sync({
    logging: false,
    force: true
}).complete(function (err) {
    if (!!err) {
        console.log(colors.red('DB: An error occurred while creating the table:'), err);
        process.exit(1);
    } else {
        //console.log(colors.green('DB: Table create!'));
        process.exit(0);
    }
});

