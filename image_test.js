var gm = require('gm');
var fs = require('fs');
var path = require('path');
var pathTestImageVertical = path.join(__dirname, '/image_test/test1.jpg');
var pathTestImage = path.join(__dirname, '/image_test/test3.jpg');
var pathResult = path.join(__dirname, '/image_test/result/');
console.log('pathResult', pathResult);
gm(pathTestImage)
    .size(function (err, size) {
        if (err) {
            console.log(err);
            return;
        }
        if (size.width > size.height) {
            h();
        } else {
            v();
        }
    });

gm(pathTestImageVertical)
    .size(function (err, size) {
        if (err) {
            console.log(err);
            return;
        }
        if (size.width > size.height) {
            h();
        } else {
            v();
        }
    });

var h = function () {
    var random = Math.random().toString();
    var resultPathImage = pathResult + random + '.jpg';
    gm(pathTestImage)
//        .resize(null, 495)
//        .gravity('Center')
//        .crop(790, 495)
//        .extent(936, 662)
        .resize(null, 890)
        .gravity('Center')
        .crop(1572, 890)
        .extent(1772, 1181)
        .size(function (err, size) {
            console.log(size);
        })
        .write(resultPathImage, function (err) {
            gm(path.join(resultPathImage))
                .composite(path.join(__dirname, '/public/img/frame_h.png'))
                .write(pathResult + 'result_horizontal.jpg', function (err) {
                    console.log(err);
                    if (!err) console.log('done 2');
                    fs.unlinkSync(resultPathImage);
                });
        });
};

var v = function () {
    var random = Math.random().toString();
    var resultPathImage = pathResult + random + '.jpg';
    gm(pathTestImageVertical)
//        .resize(375, null)
//        .gravity('Center')
//        .crop(375, 709)
//        .extent(501, 709)
        .resize(890, null)
        .gravity('Center')
        .crop(890, 1772)
        .extent(1181, 1772)
        .size(function (err, size) {
            console.log(size);
        })
        .write(resultPathImage, function (err) {
            gm(path.join(resultPathImage))
                .composite(path.join(__dirname, '/public/img/frame_v.png'))
                .write(pathResult + 'result_vertical.jpg', function (err) {
                    console.log(err);
                    if (!err) console.log('done 2');
                    fs.unlinkSync(resultPathImage);
                });
        });
};