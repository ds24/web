var socket = false;
var showDebug = false;
setTimeout(function () {
    showDebug = true;
}, 500);
var emit = {
    questConnection: {
        "boolean": true
    },
    startQuest: {
        "boolean": true
    },
    showPhotoTask: {
        "photoScreenId": 1,
        "time": "2:30"
    },
    startPhotoTask: {
        "photoScreenId": 1,
        "time": "2:30"
    },
    finishPhotoTask: {
        "photoScreenId": 1,
        "userId": 15,
        "status": true,
        "time": "1:04",
        "photo": "/test-photo/1_1.jpg",
        "photo200": "/test-photo/1_1--200.jpg",
        "photo500": "/test-photo/1_1--500.jpg",
        "photoFrame": "/test-photo/1_1--frame.jpg",
        "photoExtension": "jpg",
        "size": {
            "width": 2000,
            "height": 1000
        },
        "horizontal": true
    },
    showSuccessResult: {
        "discount": 50
    },
    showAllSuccessResult: {
        "boolean": true
    },
    showBad1Result: {
        "boolean": true
    },
    showBad2Result: {
        "boolean": true
    },
    showBadResult: {
        "boolean": true
    },
    showEmail: {
        "email": "user@mail.com"
    },
    showPrint: {
        "boolean": true
    },
    showFinishScreen: {
        "boolean": true
    },
    disconnectApp: {
        "boolean": true
    }
};
$(function () {
    $('.name').each(function () {
        var $this = $(this);
        var input = $this.find('input');
        var name = $this.attr('class').substr(14);
        input.val(name);
        var obj = getObjByName(name);
        $this.attr('onclick', name + '();');
        $this.append('<pre>' + name + '</pre>');
        $this.append('<pre class="json"><div class="json-format">' + library.json.prettyPrint(obj) + '</div></pre>');
    });
});

var getObjByName = function (name) {
    var obj = emit[name];
    if (obj.boolean) {
        obj = obj.boolean;
    }

    if (showDebug) {
        console.log('.............................');
        console.log('emit', name);
        console.log(obj);
    }

    return obj;
};

var questConnection = function () {
    socket = io();
    socket.on('connect', function () {
        console.info('connect');
    });
    socket.on('addUserSuccess', function (id) {
        console.info('addUserSuccess', id);
        emit.finishPhotoTask.userId = id;
    });
    socket.on('questDisconnect', function () {
        alert('questDisconnect');
        location.reload();
    });
    socket.on('disconnect', function () {
        alert('disconnect');
        location.reload();
    });
    socket.emit('questConnection', true);
};

var startQuest = function () {
    var name = 'startQuest';
    var obj = getObjByName(name);
    socket.emit(name, obj);
};

var showPhotoTask = function () {
    var name = 'showPhotoTask';
    var obj = getObjByName(name);
    socket.emit(name, obj);
};

var startPhotoTask = function () {
    var name = 'startPhotoTask';
    var obj = getObjByName(name);
    socket.emit(name, obj);
};

var finishPhotoTask = function () {
    var name = 'finishPhotoTask';
    var obj = getObjByName(name);
    socket.emit(name, obj);
    emit.finishPhotoTask.photoScreenId++;
};

var showSuccessResult = function () {
    var name = 'showSuccessResult';
    var obj = getObjByName(name);
    socket.emit(name, obj);
};

var showBadResult = function () {
    var name = 'showBadResult';
    var obj = getObjByName(name);
    socket.emit(name, obj);
};


var showEmail = function () {
    var name = 'showEmail';
    var obj = getObjByName(name);
    socket.emit(name, obj);
};

var showPrint = function () {
    var name = 'showPrint';
    var obj = getObjByName(name);
    socket.emit(name, obj);
};

var showAllSuccessResult = function () {
    var name = 'showAllSuccessResult';
    var obj = getObjByName(name);
    socket.emit(name, obj);
};

var showFinishScreen = function () {
    var name = 'showFinishScreen';
    var obj = getObjByName(name);
    socket.emit(name, obj);
};
var showBad1Result = function () {
    var name = 'showBad1Result';
    var obj = getObjByName(name);
    socket.emit(name, obj);
};
var showBad2Result = function () {
    var name = 'showBad2Result';
    var obj = getObjByName(name);
    socket.emit(name, obj);
};
var disconnectApp = function () {
    var name = 'disconnectApp';
    var obj = getObjByName(name);
    socket.emit(name, obj);
};

window.onbeforeunload = function () {
    if (socket) {
        console.info('emit', 'disconnectApp');
        socket.emit('disconnectApp', true);
    }
};