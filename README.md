```
npm i -g gulp bower
npm i
bower i
gulp build
node app.js
```


```
npm i pm2 -g
DEBUG=socket.io*,express* pm2 start app.js -e logs/soc.log -o logs/out.log --name web --no-daemon

DEBUG=socket.io*,express* node app.js >> log.txt
```

###windows
```
set DEBUG=socket.io*,express* & pm2 start app.js -e logs/soc.log -o logs/out.log --name web --no-daemon

set DEBUG=socket.io*,express* & node app.js >> log.txt
```


###pm2 docs

https://github.com/Unitech/PM2/blob/development/ADVANCED_README.md#raw-examples

```
C:\Users\User1\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\AutoRunApp.bat
```