@echo off
ping 127.0.0.1 -n 5 > nul
:1
cd c:\web
git pull
set DEBUG=socket.io*,express* & pm2 start app.js -e logs/soc.log -o logs/out.log --name web --no-daemon
goto 1