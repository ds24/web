####Api есть на HTTP и на WebSocket.


api url: `http://128.199.37.142:3000/` (http)

api url: `ws://128.199.37.142:3000/` (WebSocket)

Для websocket используем библотеку [socket.io-client.java](https://github.com/nkzawa/socket.io-client.java), оф сайт http://socket.io/docs/

####Страницы:

http://128.199.37.142:3000/ - Окно монитора


##Http:


#####POST `/api/v1/quest/upload`

Загружает картинку на сервер и ресайзит по высоте:

Input:
* userId
* photoId (номер фото от 1 до 5)
* file

Output:
~~~ json
{
  "status": "ok",
  "image": "/upload/quest/1_1.jpg",
  "image200": "/upload/quest/1_1--200px.jpg",
  "image500": "/upload/quest/1_1--500px.jpg",
  "photoExtension": "jpg"
}
~~~



#####POST `/api/v1/quest/getImage`

Получить все картинки по id юзера

Input:
* userId

Output
~~~ json
[
  {
    "id": 1,
    "user_id": 1,
    "taskType": "quest",
    "time": "4:58",
    "photo": "/upload/quest/1_1.jpg",
    "photoExtension": "jpg",
    "photoNumber": 1,
    "createdAt": "2015-01-31T10:56:32.000Z",
    "updatedAt": "2015-01-31T10:56:32.000Z",
    "photo200": "/upload/quest/1_1--200px.jpg",
    "photo500": "/upload/quest/1_1--500px.jpg"
  },
  {"commentTest": "  # И ТАКИХ ЕЩЕ 5, если все 5 загружены"}
]
~~~

#####POST `/api/v1/quest/emailSend`

Добавляет в базу очереди на отправку

Input:
* userId
* email
* photoId (уникальный id фото)

Output:
~~~ json
{
  "status": true
}
~~~

#####POST `/api/v1/quest/printAdd`

Отправялет фото на печать
Input:
* userId
* photoId (уникальный id фото)

Output:
~~~ json
{
  "status": true
}
~~~

###WebSocket

####Отправляем:

`questConnection` - говорим что приконектился юзер с приложения
`startQuest` - говорим что квест начался

####Слушаем:
`addUserSuccess` - Возвращает userId

`questDisconnect` - Если юзер пробует конектится, а другой юзер уже есть, то нужно вывести алетр и сказать что конектится можно только одному юpеру

####Отладка: (emit)

`closeApp` - Перезагрузит веб сервер и почистит базу (2-3 секунды)


----


###Description

#####1
При загрузки приложения ты конектишься к вебсокету:
и показываешь кнопку запустить, как только он нажмет на нее меняется экран на задание и делается emit к серверу
~~~ js
socket.emit('questConnection', true);
~~~

#####2
Появляется задание.

`screenObj` - это объект в котором я хранил id юзера, массив истории перемещения по экранам (у каждого экрана у меня был свой уникальный id)(но ты наверно можешь историю не отправлять), номер последнего фото которое он сделал, пример

~~~ js
var screenObj = {
    id: 1,
    photoScreenNum: 0,
    history: [1]
};
~~~

~~~js
socket.emit('startQuest', true);
~~~

видео к первым двум шагам http://a.pomf.se/mfooxn.mp4

#####3

Юзер жмет старт, мы
~~~ js
socket.emit('startQuest', true);
~~~
`startQuest` ответит в `addUserSuccess`, он вернет id пользователя, добавим id в объект `screenObj`

#####4
Появляется экран с 1 заданием, говорим монитору показать его, делаем emit `questSendStatus` с таким объектом:
~~~json
{
  "type": "goPhotoScreen",
  "screenObj": {
    "id": 1,
    "photoScreenNum": 1
  }
}
~~~
Как только юзер нажал выбрать картинку, запускаем таймер и делаем emit `questSendStatus`
~~~json
{
  "type": "startPhotoTask",
  "screenObj": {
    "id": 1,
    "photoScreenNum": 1
  }
}
~~~

Как только юзер выберет картинку, остававливаем время и отправляем картинку на сервер [post-apiv1questupload](#dsa)
как придет ответ, говорим экрану остановить время и показать новую картинку

делаем emit `questAddImage`, для добавления картинки в базу
Если юзер не успел за 5 минут то
time = "0"
~~~json
{
  "time": "4:58",
  "userId": 1,
  "photo": "/upload/quest/1_1.jpg",
  "photoExtension": "jpg",
  "photoNumber": 1,
  "peopleCount": 3
}
~~~
говорим экрану остановить время и показать новую картинку, делаем emit `questSendStatus`
Если юзер не успел за 5 минут то
data.time = "0"
data.status = false
~~~json
{
  "type": "endPhotoTask",
  "data": {
    "time": "4:58",
    "userId": 1,
    "photo": "/upload/quest/1_1.jpg",
    "status": true
  },
  "screenObj": {
    "id": 1,
    "photoScreenNum": 1
  }
}
~~~

И так 5 раз, после 5 картинки показываем скидку, за каждую вовремя загруженую картинку 5%, т.е. если все скидка 25%, если 3 то 15%

Если пользователь после загрузки нажал не продолжить а завершить, показываем ему экран с его скидкой и делаем emit:
~~~js
socket.emit('questShowDiscount', 25);
~~~

видео http://a.pomf.se/ynjvys.mp4


#####5


После скидки можно будет выбрать правила, призы или галерею, с правилами и призами понятно это будут просто страницы с описание.

Для того чтобы вывыести галерею используйте метод [getImage](#post-apiv1questgetimage), для выборки всех фото. Как выглядит галерея есть в тз.

Человек может выбрать определенное фото, тоже есть в тз, он может отправить фото на email ([getImage](#post-apiv1questemailsend)) или отправить на печать ([getImage](#post-apiv1questprintadd)) ,  это этого используйте соотвествуещие методы.

видео http://a.pomf.se/ildlij.mp4

#####6

При каждом выходе делай emit `disconnectApp`
Это позволит правильно посчиатать клиентов и не выкидывать если клиентов больше одного, если все равно есть проблемы то `closeApp`