var shell = require('shelljs');
var db = require('../models/models');
var _ = require('lodash');

exports.admin = function (req, res) {
    res.redirect('/admin/user');
    res.render('admin/index', {
        pageTitle: 'Home'
    });
};


exports.user = function (req, res) {
    var sql = 'SELECT si.id, si.email, task_results.time, task_results.photo, task_results.photoExtension, task_results.photoNumber ' +
        'FROM users as si ' +
        'LEFT JOIN task_results ' +
        'ON task_results.user_id = si.id';
    db.sequelize.query(sql, null, {raw: true}).success(function (result) {
        var data = {};
        result.map(function (elem) {
            data[elem.id] = {
                data : []
            };
        });
        result.map(function (elem) {
            data[elem.id].data.push(elem);
            data[elem.id].id = elem.id;
            data[elem.id].email = elem.email;
        });

        res.render('admin/user', {
            pageTitle: 'user page',
            data: data
        });
    });
};

exports.email = function (req, res) {
    db.EmailTaskModel.findAll().then(function (result) {
        res.render('admin/email', {
            pageTitle: 'Email page',
            data: result
        });
    });
};

exports.print = function (req, res) {
    db.PrintTask.findAll().then(function (result) {
        res.render('admin/print', {
            pageTitle: 'Print page',
            data: result
        });
    });
};

exports.photo = function (req, res) {
    db.TaskResultModel.findAll().then(function (result) {
        res.render('admin/photo', {
            pageTitle: 'Photo page',
            data: result
        });
    });
};


exports.git = function (req, res) {
    var sd = shell.exec("git log --all");
    res.send(sd.output)
};