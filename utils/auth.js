var basicAuth = require('basic-auth');
var config = require('./../config');

module.exports = function (req, res, next) {
    function unauthorized(res) {
        res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
        return res.send(401);
    }

    var user = basicAuth(req);

    if (!user || !user.name || !user.pass) {
        return unauthorized(res);
    }

    if (user.name === config.admin.login && user.pass === config.admin.password) {
        return next();
    } else {
        return unauthorized(res);
    }
};