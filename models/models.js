var config = require('./../config');
var colors = require('colors');
var Sequelize = require('sequelize');

var sequelize = new Sequelize(config.database.databaseName, config.database.user, config.database.password);
var User = sequelize.define('user', {
    taskType: Sequelize.STRING,
    email: Sequelize.STRING
});

var TaskResult = sequelize.define('task_result', {
    user_id: Sequelize.INTEGER,
    taskType: Sequelize.STRING,
    time: Sequelize.STRING,
    photo: Sequelize.STRING,
    photoExtension: Sequelize.STRING,
    photoNumber: Sequelize.INTEGER,
    horizontal: Sequelize.BOOLEAN,
    width: Sequelize.INTEGER,
    height: Sequelize.INTEGER,
    peopleCount: Sequelize.INTEGER
});

var EmailTask = sequelize.define('email_task', {
    user_id: Sequelize.INTEGER,
    photo_id: Sequelize.INTEGER,
    email: Sequelize.STRING,
    status: Sequelize.BOOLEAN,
    all: Sequelize.BOOLEAN
});

var PrintTask = sequelize.define('print_task', {
    user_id: Sequelize.INTEGER,
    photo_id: Sequelize.INTEGER
});

sequelize.authenticate().complete(function (err) {
    if (!!err) {
        console.log(colors.red('DB: Unable to connect to the database:'), err);
        process.exit(1);
    }
    console.log(colors.green('DB: Connection has been established successfully!'))
});

if (config.dbForce) {
    console.log('DB: force schema');
    sequelize.sync({
        logging: false,
        force: true
    }).complete(function (err) {
        if (!!err) {
            console.log(colors.red('DB: An error occurred while creating the table:'), err);
            process.exit(1);
        } else {
            //console.log(colors.green('DB: Table create!'));
        }
    });
}


exports.Sequelize = Sequelize;
exports.sequelize = sequelize;
exports.UserModel = User;
exports.TaskResultModel = TaskResult;
exports.PrintTask = PrintTask;
exports.EmailTaskModel = EmailTask;