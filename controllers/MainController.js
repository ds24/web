var fs = require('fs');
var gm = require('gm');
var config = require('./../config');
var path = require('path');
var request = require('request');
var async = require("async");
var shell = require('shelljs');
var _ = require("lodash");

var db = require('./../models/models');

var composeImage = function (file, body, callback) {
    var rootPath = process.cwd();
    var filePath = rootPath + '/' + file.path;
    var imageName = body.userId.toString() + '_' + body.photoId.toString() + '--frame';
    var pathResult = rootPath + path.join('/public/upload/quest/');
    var resultPathPhoto = pathResult + imageName + '.' + file.extension;
    var composeImage = path.join(rootPath, '/public/img/frame_v.png');
    var horizontal = false;
    var resizeWidth = 890;
    var resizeHeight = null;
    var cropWidth = 890;
    var cropHeight = 1772;
    var extentX = 1181;
    var extentY = 1772;
    gm(filePath)
        .size(function (err, size) {
            console.log('composeImage get size', err);
            if (err) {
                return;
            }
            if (size.width > size.height) {
                console.log('size.width > size.height !!!', size.width > size.height);
                composeImage = path.join(rootPath, '/public/img/frame_h.png');
                horizontal = true;
                resizeWidth = null;
                resizeHeight = 890;
                cropWidth = 1572;
                cropHeight = 890;
                extentX = 1772;
                extentY = 1181;
            }
            gm(filePath)
                .resize(resizeWidth, resizeHeight)
                .gravity('Center')
                .crop(cropWidth, cropHeight)
                .extent(extentX, extentY)
                .write(resultPathPhoto, function (err) {
                    console.log('composeImage write err', err);
                    console.log('resultPathPhoto', resultPathPhoto);
                    gm(resultPathPhoto)
                        .composite(composeImage)
                        .write(resultPathPhoto, function (err) {
                            console.log('composite err', err);
                            callback();
                        });
                });
        });
};

exports.home = function (req, res) {
    res.render('home', {
        pageTitle: 'Home'
    });
};

exports.quest = function (req, res) {
    res.render('quest', {
        pageTitle: 'Quest app'
    });
};

exports.upload = function (req, res) {
    console.log('upload', req.body, req.files);
    console.log('req.body.length', _.size(req.body));
    if (_.size(req.body) < 2) {
        res.send({
            status: false,
            body: false
        });
        return false;
    }
    if (_.size(req.files) < 1) {
        res.send({
            status: false,
            file: false
        });
        return false;
    }
    var userId = req.body.userId;
    var photoId = req.body.photoId;
    var file = req.files.file;
    var rootPath = process.cwd();
    var oldPath = rootPath + '/' + file.path;
    var imageName = userId.toString() + '_' + photoId.toString();
    var newPath = rootPath + '/public/upload/quest/' + imageName + '.' + file.extension;
    var sizeObject = {};
    var horizontal = false;
    async.waterfall([
        function (callback) {
            console.log("upload image newPath", newPath);
            if (photoId == 2 || photoId == 5) {
                fs.renameSync(oldPath, newPath);
                horizontal = true;
                responseSend();
            } else {
                composeImage(file, req.body, function () {
                    callback(null, 'ok');
                    fs.renameSync(oldPath, newPath);
                });
            }
        },
        function (arg1, callback) {
            if (config.resizeImage) {
                var resizeImage = function (elem, callback) {
                    var resizeName = rootPath + '/public/upload/quest/' + imageName + '--' + elem + 'px.' + file.extension;
                    gm(newPath)
                        .resize(null, elem)
                        .size(function (err, size) {
                            sizeObject = size;
                            if (size.width > size.height) {
                                horizontal = true;
                            }
                        })
                        .write(resizeName, function (err) {
                            console.log('resizeImage', err, newPath);
                            callback(null, resizeName);
                        });
                };
                async.map([200, 500], resizeImage, function (err, results) {
                    callback(null, 'ok');
                });
            } else {
                callback(null, 'ok');
            }
        }
    ], function (err, result) {
        console.log("rend ok");
        responseSend();
    });

    var responseSend = function () {
        console.log('UPLOAD DEBUG: responseSend');
        var resObj = {
            status: 'ok',
            photo: '/upload/quest/' + imageName + '.' + file.extension,
            photo200: '/upload/quest/' + imageName + '--200px.' + file.extension,
            photo500: '/upload/quest/' + imageName + '--500px.' + file.extension,
            photoFrame: '/upload/quest/' + imageName + '--frame.' + file.extension,
            photoExtension: file.extension,
            size: sizeObject,
            horizontal: horizontal
        };
        console.log('UPLOAD DEBUG: responseSend ojb', resObj);
        res.send(resObj);
    };
};

exports.emailSend = function (req, res) {
    if (_.size(req.body) < 1) {
        res.send({
            status: false
        });
        return false;
    }
    var data = req.body;
    console.log('exports.emailSend', data);
    db.EmailTaskModel.create({
        email: data.email,
        user_id: data.userId,
        photo_id: data.photoId,
        status: false,
        all: data.all
    }).then(function (result) {
        db.UserModel.update({email: req.body.email}, {where: {id: req.body.userId}}).then(function (err) {
            sendEmail();
            res.send({
                status: true
            });
        });
    });
};

exports.printAdd = function (req, res) {
    var data = req.body;
    var rootPath = process.cwd();
    console.log('PrintTask: data', data);
    if (_.size(req.body) < 1) {
        res.send({
            status: false
        });
        return false;
    }
    db.PrintTask.create({
        user_id: data.userId,
        photo_id: data.photoId
    }).then(function (result) {
        console.log('PrintTask: db.PrintTask.create');
        db.TaskResultModel.find({
            where: {
                user_id: data.userId,
                id: data.photoId
            }
        }).then(function (result, err) {
            if (!result) {
                console.log('PrintTask: photo not found');
                res.send({
                    status: false,
                    message: "photo not found"
                });
                return;
            }
            var data = result.dataValues;
            console.log('PrintTask: photo found!!', data);
            var userId = data.user_id;
            var photoNumber = data.photoNumber;
            var pathImage = path.join('/public/upload/quest/');
            var frame = '--frame';
            if (photoNumber == 2 || photoNumber == 5) {
                frame = '';
            }
            var imageName = userId + '_' + photoNumber + frame + '.' + data.photoExtension;
            var resultPath = path.join(rootPath, pathImage, imageName);
            console.log('PrintTask resultPath: ', resultPath);
            console.log('');
            console.log('');
            var isWin = /^win/.test(process.platform);
            if (isWin) {
                var batFilePath = path.join(rootPath, '/print.bat');
                console.log('PrintTask bat file', batFilePath);
                fs.writeFile(batFilePath, config.printCommand.replace('%1', resultPath), function (err) {
                    if (err) {
                        console.log('PrintTask: ', err);
                    } else {
                        console.log("PrintTask: Bat file saved!");
                    }
                });
                var command = "cmd /c start /b " + batFilePath;
                console.log('');
                console.log('PrintTask PRINT: ');
                console.log(command);
                setTimeout(function () {
                    var out = shell.exec(command);
                    console.log('PrintTask: RUN PRINT: out', out);
                }, 500);
            }
            res.send({
                status: true,
                resultPath: resultPath
            });
        });
    });
};

exports.reboot = function (req, res) {
    process.exit(0);
};

exports.getImageByUserId = function (req, res) {
    var userId = req.body.userId;
    console.log('DEBUG: getImageByUserId', req.body);
    db.TaskResultModel.findAll({where: {user_id: userId}}).then(function (project) {
        console.log('DEBUG: getImageByUserId find', project);
        var pathName = '/upload/quest/';
        var result = project.map(function (elem) {
            var data = elem.dataValues;
            data.photo200 = pathName + userId + '_' + data.photoNumber + '--200px.' + data.photoExtension;
            data.photo500 = pathName + userId + '_' + data.photoNumber + '--500px.' + data.photoExtension;
            if (data.time == '0') {
                data.time = false;
            }
            elem.dataValues = data;
            return elem;
        });
        console.log('DEBUG: getImageByUserId response', result);
        res.send(result);
    });
};

var sendEmail = function () {
    var rootPath = process.cwd();
    db.EmailTaskModel.findAll({
        where: {
            status: false
        }
    }).then(function (result, err) {
        if (_.size(result) < 1) {
            console.log("sendEmail: email not found!");
            return false;
        }
        console.log('sendEmail: result_one', result);
        var upp = function (item, callback) {
            var data = item.dataValues;
            console.log('sendEmail: data', data);
            var email = data.email;
            var emailTaskId = data.id;
            var where = {
                user_id: data.user_id,
                id: data.photo_id
            };
            if (data.all) {
                where = {
                    user_id: data.user_id
                };
            }
            db.TaskResultModel.findAll({
                where: where
            }).then(function (result, err) {
                if (!result) {
                    console.log("image not found");
                    return;
                }
                console.log('TaskResultModel result count:', _.size(result));
                var resultArr = [];
                async.map(result, function (item, callback) {
                    resultArr.push(getDataImageFromSql(item.dataValues, email, emailTaskId));
                    setTimeout(function () {
                        callback(resultArr, null)
                    }, 1000);
                }, function (result, err) {
                    if (_.size(result) < 1) {
                        callback(null);
                    } else {
                        sendUploadImage(result);
                        callback(null);
                    }
                });
            });
        };
        async.map(result, upp, function (err, result) {
            console.log('result', result);
        });
    });

    var getDataImageFromSql = function (imageData, email, emailTaskId) {
        console.log('imageData', imageData);
        var userId = imageData.user_id;
        var photoNumber = imageData.photoNumber;
        var pathImage = path.join('/public/upload/quest/');
        var frame = '--frame';
        var photoExtension = imageData.photoExtension;
        if (photoNumber == 2 || photoNumber == 5) {
            frame = '';
        }
        var imageName = userId + '_' + photoNumber + frame + '.' + imageData.photoExtension;
        var resultPath = path.join(rootPath, pathImage, imageName);
        return {
            resultPath: resultPath,
            userId: userId,
            photoNumber: photoNumber,
            email: email,
            photoExtension: photoExtension,
            emailTaskId: emailTaskId
        }
    };

    var sendUploadImage = function (resultArr) {
        console.log('UP_EMAIL: resultArr', resultArr);
        var userId = resultArr[0].userId;
        var email = resultArr[0].email;
        var emailTaskId = resultArr[0].emailTaskId;
        var file = [];
        var oneRequestData = {
            id: userId,
            stendId: config.stendId,
            email: email
        };

        if (_.size(resultArr) > 1) {
            resultArr.map(function (imageUploadItem) {
                console.log('UP_EMAIL: imageUploadItem.resultPath > 1', imageUploadItem.resultPath);
                if (!_.isNull(imageUploadItem.photoExtension)) {
                    file.push(fs.createReadStream(imageUploadItem.resultPath));
                } else {
                    console.log('UP_EMAIL: imageUploadItem.resultPath > 1 NULL', imageUploadItem);
                }
            });
        } else {
            console.log('UP_EMAIL: imageUploadItem.resultPath', resultArr[0].resultPath);
            if (!_.isNull(resultArr[0].photoExtension)) {
                file.push(fs.createReadStream(resultArr[0].resultPath));
            } else {
                console.log('UP_EMAIL: imageUploadItem.resultPath NULL', resultArr[0]);
            }
        }

        request.post({
            url: 'https://emailservice24.com/index.php?r=fotoquest/createRecepient&key=' + config.secretKey,
            formData: oneRequestData
        }, function optionalCallback(err, httpResponse, body) {
            if (err) {
                console.log('UP_EMAIL: upload failed:', err);
                return false;
            }
            console.log('UP_EMAIL: fotoquest/createRecepient! Server responded with:', body);
            try {
                var responseJson = JSON.parse(body);
            } catch (e) {
                console.log(e);
                return false;
            }
            console.log('UP_EMAIL: responseJson', responseJson);
            var formData = {
                name: 'quest',
                id: responseJson.id,
                'file[]': file
            };
            request.post({
                url: 'https://emailservice24.com/index.php?r=fotoquest/sendFoto&key=' + config.secretKey,
                formData: formData
            }, function optionalCallback(err, httpResponse, body) {
                if (err) {
                    console.log('UP_EMAIL: sendFoto upload failed:', err);
                    return false;
                }
                console.log('UP_EMAIL: fotoquest/sendFoto!  Server responded with:', body);
                console.log('UP_EMAIL: EmailTaskModel.update #id RUN!', emailTaskId);
                db.EmailTaskModel.update({status: true}, {where: {id: emailTaskId}}).then(function (err) {
                    console.log('UP_EMAIL: EmailTaskModel.update #id ok!', emailTaskId);
                });
            });
        });
    };
};


exports.sendEmail = sendEmail;