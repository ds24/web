var express = require('express');
var exec = require('child_process').exec, child;
var models = require('./models/models');
var fs = require('fs');
var app = express();
var multer = require('multer');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var config = require('./config');
var helper = require('./utils/helper');
var colors = require('colors');
var auth = require('./utils/auth');
var MainController = require('./controllers/MainController');
var AdminController = require('./controllers/AdminController');

server.listen(config.port, function () {
    helper.clearConsole();
    console.log('Server listening at port %d', config.port);
    console.log('Open http://localhost:%d in you browser!'.green, config.port);
});
//helper.uploadPath();
app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded());

app.get('/', MainController.home);
app.get('/quest', MainController.quest);
app.post('/api/v1/quest/upload', [multer({dest: './public/upload/quest'})], MainController.upload);
app.post('/api/v1/quest/emailSend', [multer()], MainController.emailSend);
app.post('/api/v1/quest/printAdd', [multer()], MainController.printAdd);
app.post('/api/v1/quest/getImage', [multer()], MainController.getImageByUserId);
app.post('/api/v1/quest/reboot', MainController.reboot);

app.get('/admin', auth, AdminController.admin);
app.get('/admin/user', auth, AdminController.user);
app.get('/admin/email', auth, AdminController.email);
app.get('/admin/print', auth, AdminController.print);
app.get('/admin/photo', auth, AdminController.photo);
app.get('/admin/git', auth, AdminController.git);

require('./socket/MainSocket')(io, colors);
app.use(express.static(__dirname + '/public'));

setInterval(function () {
    console.log("EMAIL: check email for send");
    MainController.sendEmail();
}, 60000);