var colors = require('colors');
var tty = require('tty');
var mkdirp = require('mkdirp');

var runName = process.argv[2];
var createRunDesktopBat = function () {
    var ejs = require('ejs');
    var fs = require('fs');
    var str = fs.readFileSync(__dirname + '/windws-bat-tpl/runAppBat.ejs', 'utf8');
    var ret = ejs.render(str, {
        path: process.cwd()
    });
    fs.writeFile(process.env.home + "\\Desktop\\runApp.bat", ret, function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log("The bat file was saved!");
        }
    });
};

var clearConsole = function () {
    for (var i = 0; i < 5; i++) {
        console.log('\r\n');
    }
};

var printSocketClient = function (clients, type) {
    console.log('................................');
    if (type) {
        console.log('disconnect');
    }
    for (var index in clients) {
        var attr = clients[index];
        if (type) {
            console.log(colors.red(attr.id));
        } else {
            console.log(attr.id);
        }
    }
    console.log('................................');
};

var printSocketArrayClient = function (clients, type) {
    if (type) {
        console.log('disconnect');
    }
    clients.map(function (socket, num) {
        num++;
        console.log(' . . . . . . client #', num, ' id:',  socket.id, ' date -> ', new Date().toString());
    });
    if (clients.length < 1) {
        console.log(". . . . . .  clients not found", ' date -> ', new Date().toString());
    }
};


var uploadPath = function () {
    var path = process.cwd() + '\\public\\upload\\quest';
    mkdirp(path, function (err) {
        console.log('mkdirp err', err);
    });
};
if (runName == "createRunDesktopBat") {
    createRunDesktopBat();
}
if (runName == "clearConsole") {
    clearConsole();
}


exports.clearConsole = clearConsole;
exports.createRunDesktopBat = createRunDesktopBat;
exports.printSocketClient = printSocketClient;
exports.printSocketArrayClient = printSocketArrayClient;
exports.uploadPath = uploadPath;
