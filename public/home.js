var debug = false;
var showHeader = function (type, num) {
    var item = $('.screen-header .head');
    if (type == 'active') {
        $('.screen-header').show();
        if (num == 'all') {
            item.addClass('active');
        }
        if (typeof num == 'number') {
            item.removeClass('active');
            console.log('obj', item.eq(num - 1)[0]);
            item.eq(num - 1).addClass('active');
        }
    }
    if (type == 'disable') {
        $('.screen-header').hide();
    }
};
var showTask = function () {
    $('.screen').hide();
    showHeader('active', 'all');
    $('.screen-task').show();
};
var showBad = function () {
    console.log('showBad!!');
    showHeader('disable');
    $('.screen').hide();
    $('.screen-bad').show();
};
var showSuccess = function (discount) {
    showHeader('disable');
    var $this = $('.screen-success');
    $('.screen').hide();
    $this.find('.discount').text(discount);
    $this.show();
};
var showPrint = function () {
    showHeader('disable');
    $('.screen').hide();
    $('.screen-print').show();
};

var showEmail = function (email) {
    showHeader('disable');
    var $this = $('.screen-email');
    $('.screen').hide();
    $this.find('.name').text(email);
    $this.show();
};

var showFinish = function () {
    showHeader('disable');
    $('.screen').hide();
    $('.screen-finish').show();
};

var showPhoto = function (type, data) {
    $('.screen').hide();
    var infoHtml;
    var $screenPhoto = $('.screen-photo');
    var $screenPhotoInfo = $('.screen-photo .info');
    var $screenPhotoContent = $('.screen-photo .content');
    var photoScreenId = data.photoScreenId;
    var time = data.time;
    showHeader('active', photoScreenId);
    var contentHtml = $('.photo-content-' + photoScreenId);
    $screenPhotoContent.attr('class', 'content');
    $screenPhotoContent.addClass('num' + photoScreenId);
    $screenPhotoContent.removeClass('photo');
    $screenPhotoContent.removeClass('gif');
    $screenPhotoContent.removeClass('panorama');
    if (type == 'show') {
        infoHtml = $('.photo-info-show').html();
        $screenPhotoInfo.html(infoHtml);
        $screenPhotoContent.html(contentHtml.html());
        $screenPhotoInfo.find('.time').text(time);
        $screenPhotoInfo.find('.image').attr('src', '/img/task_image/task' + photoScreenId + '.jpg');

    }
    if (type == 'start') {
        infoHtml = $('.photo-info-start').html();
        $screenPhotoInfo.html(infoHtml);
        $screenPhotoContent.html(contentHtml.html());
        var $leftTime = $('.screen-photo .timer-left');
        var $rightTime = $('.screen-photo .timer-right');
        var $clock = $('.time').on('update.countdown', function (event) {
            var format = '%-M';
            $leftTime.text(event.strftime(format));
            format = '%S';
            $rightTime.text(event.strftime(format));
        }).on('finish.countdown', function (event) {
            console.log('finish.countdown');
        });
        var selectedDate = new Date().valueOf() + getTimeForTimer(time) - 1000;
        $clock.countdown(selectedDate.toString());

    }
    if (type == 'finish') {
        var photoClass = 'vertical';
        var photoUrl = data.photoFrame;
        if (data.horizontal) {
            photoClass = 'horizontal'
        }
        $screenPhotoContent.addClass('photo');
        if (photoScreenId == 2) {
            $screenPhotoContent.addClass('gif');
            photoClass = 'gif';
            photoUrl = data.photo;
        }
        if (photoScreenId == 5) {
            $screenPhotoContent.addClass('panorama');
            photoClass = 'panorama';
            photoUrl = data.photo;
        }
        var finishHtml = '';
        if (data.status) {
            finishHtml = $('.photo-info-finish').html();
            $screenPhotoInfo.html(finishHtml);
            $screenPhotoContent.empty().html('<div class=" ' + photoClass + ' "><img src="' + photoUrl + ' " alt=""/></div>')
        } else {
            $screenPhotoContent.removeClass('photo');
            $screenPhotoContent.removeClass('gif');
            finishHtml = $('.photo-info-finishBad').html();
            $screenPhotoInfo.html(finishHtml);
            $screenPhotoContent.html(contentHtml.html());
        }
    }
    $screenPhoto.show();
};

var getTimeText = function (time) {
    time = time.split(':');
    var left = time[0];
    var right = time[1];
    var result = '';
    console.log('left', left);
    console.log('right', right);
    if (right === '00') {
        result = left + ' мин';
    } else {
        result = left + '.' + right.substring(0, 1) + ' мин';
    }

    return result;
};

var getTimeForTimer = function (time) {
    time = time.split(':');
    var left = time[0];
    var right = time[1];
    var result = 10000;
    result = parseInt(left) * 60000;
    if (right != '00') {
        result = result + (parseInt(right) * 1000);
    }

    return result;
};

var showAllSuccessResult = function () {
    showHeader('disable');
    $('.screen').hide();
    var $this = $('.showAllSuccessResult');
    $this.show();
};

var showBad1Result = function () {
    showHeader('disable');
    $('.screen').hide();
    var $this = $('.showBad1Result');
    $this.show();
};


var showBad2Result = function () {
    showHeader('disable');
    $('.screen').hide();
    var $this = $('.showBad2Result');
    $this.show();
};

var showGalleryItem = function (data) {
    showHeader('disable');
    $('.screen').hide();
    var $this = $('.showGalleryItem');
    var $image = $this.find('img');
    $image.attr('src', data.photo);
    $this.show();
};

$(function () {
    var videoBlockElem = $('.video-block');
    var myVideo = document.getElementById("video");
    myVideo.play();
    myVideo.volume = 0;
    myVideo.loop = 1;
//    videoBlockElem.hide();
    var socket = io();
//    showTask();
//    showPhoto('show', {
//        photoScreenId: 3,
//        time: "2:30"
//    })
    socket.on('connect', function () {
        socket.emit('homeConnection', true);
    });

    socket.on('showPhotoTask', function (data) {
        console.info('showPhotoTask', data);
        showPhoto('show', data);
    });
    socket.on('startPhotoTask', function (data) {
        console.info('startPhotoTask', data);
        showPhoto('start', data);
    });
    socket.on('finishPhotoTask', function (data) {
        console.info('finishPhotoTask', data);
        showPhoto('finish', data);
    });

    // open
    socket.on('homeOpen', function (data) {
        console.info('home', data);
        videoBlockElem.hide();
        showTask();
    });
    socket.on('showSuccessResult', function (data) {
        console.info('showSuccessResult', data);
        showSuccess(data.discount);
    });
    socket.on('showBadResult', function (data) {
        console.info('showBadResult', data);
        showBad();
    });
    // other
    socket.on('showEmail', function (data) {
        console.info('showEmail', data);
        showEmail(data.email);
    });
    socket.on('showPrint', function (data) {
        console.info('showPrint', data);
        showPrint();
    });
    socket.on('showFinishScreen', function (data) {
        console.info('showFinishScreen', data);
        showFinish();
    });
    socket.on('homeNewConnect', function (data) {
        console.log('homeNewConnect', data);
        videoBlockElem.hide();
        showTask();
    });
    socket.on('showAllSuccessResult', function (data) {
        console.log('showAllSuccessResult', data);
        showAllSuccessResult();
    });
    socket.on('showBad1Result', function (data) {
        console.log('showBad1Result', data);
        showBad1Result();
    });
    socket.on('showBad2Result', function (data) {
        console.log('showBad2Result', data);
        showBad2Result();
    });
    socket.on('showGalleryItem', function (data) {
        console.log('showGalleryItem', data);
        showGalleryItem(data);
    });
    socket.on('homeLastQuestExit', function (data) {
        console.log('homeLastQuestExit', data);
        videoBlockElem.show();
        location.reload();
    });
    socket.on('disconnect', function (data) {
        videoBlockElem.show();
    });
});