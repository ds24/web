var gulp = require('gulp');
var stylus = require('gulp-stylus');
var nib = require('nib');
var browserSync = require('browser-sync');
var filter = require('gulp-filter');
var reload = browserSync.reload;
var sourcemaps = require('gulp-sourcemaps');

var stylusPath = './stylus/*.styl';

gulp.task('browser-sync', function () {
    browserSync.init(null, {
        proxy: "http://localhost:3000",
        files: ["public/**/*.*"],
        browser: "google chrome",
        port: 7000
    });
});

gulp.task('stylus', function () {
    gulp.src(stylusPath)
        .pipe(sourcemaps.init())
        .pipe(stylus())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./public/css'));
});

gulp.task('watch', function () {
    return gulp.watch([stylusPath], ['stylus']);
});

gulp.task('default', [
    'browser-sync',
    'stylus',
    'watch'
]);

gulp.task('build', ['stylus']);