var fs = require('fs');
var path = require('path');
var config = require('./config');
var shell = require('shelljs');
var args = process.argv.slice(2);
var rootPath = process.cwd();
console.log('args', args);
if (args.length < 1) {
    console.log("using:");
    console.log("node print_test \"D:\\image.jpg\"");
    process.exit(0);
}

console.log('');
fs.writeFile("./print.bat", config.printCommand.replace('%1', args[0]), function(err) {
    if(err) {
        console.log(err);
    } else {
        console.log("Bat file saved!");
    }
});
var command = "cmd /c start /b " + path.join(rootPath, '/print.bat');
console.log('');
console.log('RUN: ');
console.log(command);
setTimeout(function () {
    var out = shell.exec(command);
    console.log('out', out);
}, 500);