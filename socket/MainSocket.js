var db = require('./../models/models');
var helper = require('./../utils/helper');
var _ = require('lodash');

/**
 * 1 - quest
 * 2 - music
 * 3 - home
 */
function getName(num) {
    if (num == 1) {
        return 'quest';
    }
    if (num == 2) {
        return 'music';
    }
    if (num == 3) {
        return 'home';
    }
}

var usersQuestCount = 0;
var usersMusicCount = 0;
var usersHomeCount = 0;
var userUnicId = 0;
var questClients = [];
var homeClients = [];
var lastId = false;
module.exports = function (io, colors) {
    io.on('connection', function (socket) {
        socket.username = false;
        var clientIpAddress = socket.request.socket.remoteAddress;

        console.log(colors.bgBlue(colors.white("clientIpAddress", clientIpAddress)));

        /**
         * quest logic
         */
        socket.on('questConnection', function () {
            console.log('');
            console.log('');
            console.log(colors.green('[[ questConnection ]]'), socket.id);
            if (questClients.length < 1) {
                userUnicId = socket.id;
            }
            questClients.push(socket);
            socket.username = 1;
            helper.printSocketArrayClient(questClients);

            // выкидываем если юзер второй или больше
            // юзер может быть только один
            console.log('questClients.length', questClients.length);
            if (questClients.length > 1) {
                console.log("MANY CLIENTS!!!");
                console.log(colors.red('[[ disconnectApp ]]'));
                io.sockets.connected[lastId].disconnect();
                io.sockets.emit('homeNewConnect', true);
                var index = questClients.indexOf(1);
            }
            lastId = socket.id;

            // при конекте к квесту говорим экрану убрать видео
            socket.broadcast.emit('homeOpen', true);
            console.log('questConnection: username ->', colors.green(getName(socket.username)), ' questClientsCount ->', questClients.length);
        });

        socket.on('startQuest', function (data) {
            db.UserModel.create({
                taskType: "quest",
                email: ""
            }).then(function (result) {
                socket.emit('addUserSuccess', result.values.id);
                socket.emit('homeOpen', true);
            });
        });

        // PHOTO
        socket.on('showPhotoTask', function (data) {
            socket.broadcast.emit('showPhotoTask', data);
        });
        socket.on('startPhotoTask', function (data) {
            socket.broadcast.emit('startPhotoTask', data);
        });
        socket.on('finishPhotoTask', function (data) {
            console.log('data.size', data.size);
            console.log('data.size typeof', typeof data.size);
            if (_.size(data.size) < 1) {
                data.size = {};
            }
            if (_.size(data.photoExtension) > 0) {
                db.TaskResultModel.create({
                    user_id: data.userId,
                    taskType: 'quest',
                    time: data.time,
                    photo: data.photo,
                    photoNumber: data.photoScreenId,
                    peopleCount: data.peopleCount,
                    horizontal: data.horizontal,
                    width: data.size.width,
                    height: data.size.height,
                    photoExtension: data.photoExtension
                }).then(function (result) {
                    console.log('questAddImage');
                });
            }
            socket.broadcast.emit('finishPhotoTask', data);
        });

        // STATUS
        socket.on('showSuccessResult', function (data) {
            socket.broadcast.emit('showSuccessResult', data);
        });
        socket.on('showBadResult', function (data) {
            socket.broadcast.emit('showBadResult', data);
        });
        socket.on('showEmail', function (data) {
            socket.broadcast.emit('showEmail', data);
        });
        socket.on('showPrint', function (data) {
            socket.broadcast.emit('showPrint', data);
        });
        socket.on('showFinishScreen', function (data) {
            socket.broadcast.emit('showFinishScreen', data);
        });
        socket.on('showAllSuccessResult', function (data) {
            socket.broadcast.emit('showAllSuccessResult', data);
        });
        socket.on('showBad1Result', function (data) {
            console.log('showBad1Result', data);
            socket.broadcast.emit('showBad1Result', data);
        });
        socket.on('showBad2Result', function (data) {
            console.log('showBad2Result', data);
            socket.broadcast.emit('showBad2Result', data);
        });
        socket.on('showGalleryItem', function (data) {
            console.log('showGalleryItem', data);
            socket.broadcast.emit('showGalleryItem', data);
        });

        /**
         * home logic
         */
        socket.on('homeConnection', function () {
            socket.username = 3;
            console.log('homeConnection: socket.username ->', getName(socket.username));
            console.log('homeConnection: questClientsCount ->', questClients.length);
            if (questClients.length > 0) {
                io.sockets.connected[socket.id].emit('homeOpen', true);
            }
        });

        /**
         * other logic
         */
        socket.on('disconnectApp', function () {
            console.log('');
            if (socket.username == 1) {
                // выходит последний юзер с квеста, показываем видео на мониторе
                if (questClients.length == 1) {
                    io.sockets.emit('homeLastQuestExit', true);
                }
            }
            questClients.splice(questClients.indexOf(socket), 1);
            helper.printSocketArrayClient(questClients);
            console.log(colors.red('disconnect: username ->'), getName(socket.username), ' ', socket.id);
            socket.disconnect();
        });

        socket.on('disconnect', function () {
            console.log(colors.red('disconnectApp'), socket.id);
            questClients.splice(questClients.indexOf(socket), 1);
            helper.printSocketArrayClient(questClients);
        });
        socket.on('closeApp', function () {
            process.exit(0);
        });
    });
};

setInterval(function () {
    helper.printSocketArrayClient(questClients);
}, 15000);
